from flask import Flask, request, jsonify, render_template
import hashlib
import urllib.parse
from random import randint
import threading
import time
import requests
import string
import random
app = Flask(__name__)


REDIRECT_TO_OK = True


def g2apay_sha256(*a):
    s = ''.join(a)
    return hashlib.sha256(s.encode('utf-8')).hexdigest()


def response(data):
    a = []
    for key, value in data:
        k = urllib.parse.quote_plus(key)
        v = urllib.parse.quote_plus(value)
        a.append(k+'='+v)
    return '&'.join(a)




ApiSecret = 'secret'


TOKENS = {
}

TRANSACTIONS = {
}

#https://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
def token_generator(size=13, chars=string.ascii_lowercase + string.digits):
   return ''.join(random.choice(chars) for _ in range(size))


# https://checkout.pay.g2a.com/index/createQuote
# https://checkout.test.pay.g2a.com/index/createQuote
# https://pay.g2a.com/documentation/payment-creation
@app.route("/index/createQuote", methods=['POST'])
def createQuote():

    token = token_generator()

    print(request.form)

    data = {
        'api_hash': request.form['api_hash'],
        'hash': request.form['hash'],
        'order_id': request.form['order_id'],
        'amount': request.form['amount'],
        'currency': request.form['currency'],
        'url_failure': request.form['url_failure'],
        'url_ok': request.form['url_ok'],
        # 'cart_type': request.form['cart_type'], # otpional

        'item_sku': request.form['items[0][sku]'],
        'item_name': request.form['items[0][name]'],
        'item_amount': request.form['items[0][amount]'],
        'item_qty': request.form['items[0][qty]'],
        # 'item_extra': request.form['items[0][extra]'], # otpional
        # 'item_type': request.form['items[0][type]'], # otpional
        'item_id': request.form['items[0][id]'],
        'item_price': request.form['items[0][price]'],
        'item_url': request.form['items[0][url]'],
    }

    TOKENS[token] = data

    hash_should = g2apay_sha256(data['order_id'], data['amount'], data['currency'], ApiSecret)

    if(data['hash'] != hash_should):
        return jsonify({
            'status': False,
            'error': True,
            'message': 'Invalid hash',
            'title': None,
            'code': 'CO0000',
            'params': {},
            'details': None,
        })

    return jsonify({
        'status': 'ok',
        'token': token, # '54f863189b6db'
    })


# https://checkout.pay.g2a.com/index/gateway?token=54f863189b6db
@app.route("/index/gateway", methods=['GET'])
def gateway():
    token = request.args.get('token')
    transactionId = randint(0, 10000)
    TRANSACTIONS[transactionId] = TOKENS[token]
    TRANSACTIONS[transactionId]['transactionId'] = transactionId
    return render_template('gateway.html',
        url_ok=TOKENS[token]['url_ok']+'?transactionId={}'.format(transactionId),
        url_failure=TOKENS[token]['url_failure'],
    )


@app.route("/rest/transactions/<transactionId>")
def rest(transactionId):
    return jsonify({
        "status": "complete"
    })

